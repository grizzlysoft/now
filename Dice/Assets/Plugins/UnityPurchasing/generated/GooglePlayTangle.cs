#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("+RSN7KAc+HDf+ZaTN7XCNpP0r0+OdHF6lFhYhOS9ozOn+zu/m4G/X3Yu/TGw/bLKiDDluBkjPLTqwir//03O7f/CycblSYdJOMLOzs7Kz8wTAe197uxl0zPgeMSrPywWSUglP/5hk7D5gEWoifDG71AlNe93VujdoT+hbrcPk4Mz8l9DMDs/3lX8/tq2EWR7+KbVthC6WVrvUtHZcBIVL12b+2ZbdZJ/KRZN2ZgRgbZwweL5F1aXkwbl/YPu/8vDycnW24arE4UZEtIwx0tX5iw2F8rWaUy0YBqhzE3OwM//Tc7FzU3Ozs8eY7jo1YCx4cBjxSddQR47w4PJ0dTUWoCa1B64LMsLJRsinBCxok5X50eZL0K3xSZzY1eBbubYgs3Mzs/O");
        private static int[] order = new int[] { 7,5,6,7,6,8,6,7,11,13,12,11,12,13,14 };
        private static int key = 207;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
