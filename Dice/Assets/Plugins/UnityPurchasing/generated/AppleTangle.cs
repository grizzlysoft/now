#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("1OoDM1J6Yc03j8C6ewgQT7CBaLTbx86L+cTE34vo6pu1vKabnZufmdnKyN/CyM6L2N/K387GzsXf2IWbnTLnhtMcRicwd1jcMFndedyb5Gru1bTnwPs96iJv38mguyjqLJghKqxH1pIoIPiLeJNvGhQx5KHAVIBXrZukraj+triqqlSvrpuoqqpUm7aHi8jO2d/CzcLIyt/Oi9vEx8LI0o2bj62o/q+guLbq29vHzovoztnfz56IvuC+8rYYP1xdNzVk+xFq8/sACNo57Pj+agSE6hhTUEjbZk0I59vHzovoztnfws3CyMrfwsTFi+rem7qtqP6vobih6tvbx86L4sXIhZqvrbip/viauJu6raj+r6G4oerb26atooEt4y1cpqqqrq6rqCmqqqv3YrLZXvalftT0MFmOqBH+JOb2plqLxM2L38POi9/DzsWLytvbx8LIyh6RBl+kpas5oBqKvYXffpemcMm9x86L4sXIhZqNm4+tqP6voLi26tvRmymq3Zulraj+tqSqqlSvr6ipqovKxc+LyM7Z38LNwsjK38LExYvbGpvzR/GvmSfDGCS2dc7YVMz1zhfJx86L2N/Kxc/K2c+L387ZxtiLypsprxCbKagIC6ipqqmpqqmbpq2iILIidVLgx16sAImbqUOzlVP7oni9m7+tqP6vqLim6tvbx86L+cTE3/nOx8LKxcjOi8TFi9/DwtiLyM7ZtC4oLrAyluycWQIw6yWHfxo7uXOL6OqbKaqJm6atooEt4y1cpqqqqswkox+LXGAHh4vE2x2UqpsnHOhk3NyFytvbx86FyMTGhMrb28fOyMric900mL/OCtw/YoapqKqrqggpqimqq62igS3jLVzIz66qmypZm4GtpDaWWIDig7FjVWUeEqVy9bd9YJYk2CrLbbDwooQ5GVPv41vLkzW+XhywFjjpj7mBbKS2HeY39chj4Cu8hesNXOzm1KP1m7StqP62iK+zm72tqP62pa+9r7+Ae8LsP92iVV/AJrQ6cLXs+0CuRvXSL4ZAnQn85/5Hnpman5uYnfG8ppiem5mbkpman5uj9ZspqrqtqP62i68pqqObKaqvm9/DxNnC39KavZu/raj+r6i4purbFV/YMEV5z6Rg0uSfcwmVUtNUwGPyDK6i17zr/bq133gcIIiQ7Ah+xGvImNxckayH/UBxpIqlcRHYsuQegS3jLVymqqqurqubyZqgm6KtqP6YnfGbyZqgm6KtqP6vrbip/viauN/CzcLIyt/Oi8nSi8rF0ovbytnfA3fViZ5hjn5ypH3AfwmPiLpcCgfSi8rY2N7GztiLysjIztvfysXIziu/gHvC7D/dolVfwCaF6w1c7ObUj0lAehzbdKTuSoxhWsbTRkwevLyjgK2qrq6sqaq9tcPf39vYkYSE3JaNzIshmMFcpilkdUAIhFL4wfDPws3CyMrfwsTFi+re38PE2cLf0ppyndRqLP5yDDISmelQc37aNdUK+T410acP7CDwf72cmGBvpOZlv8J6hJsqaK2jgK2qrq6sqambKh2xKhjFz4vIxMXPwt/CxMXYi8TNi97Yzq6rqCmqpKubKaqhqSmqqqtPOgKi+wEhfnFPV3uirJwb3t6K");
        private static int[] order = new int[] { 17,6,46,48,25,51,29,44,10,20,24,25,20,46,47,19,41,23,48,28,45,26,42,23,41,45,38,49,46,38,36,49,34,42,55,59,49,49,46,44,42,51,50,52,57,46,51,57,54,50,55,53,59,56,56,58,56,58,59,59,60 };
        private static int key = 171;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
