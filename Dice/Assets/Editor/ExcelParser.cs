﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

using Microsoft.CSharp;
using System.CodeDom.Compiler;

/* NPOI 라이브러리
Apache POI는 다음과 같은 하위 컴포넌트로 구성되어 있다.

POIFS(Poor Obfuscation Implementation File System) : 마이크로소프트 오피스의 OLE 2 Compound document 파일 포맷을 읽고 쓰는 컴포넌트. 모든 오피스 파일 포맷은 OLE2 방식이므로 하위 모든 컴포넌트의 기반이 된다.
HSSF(Horrible SpreadSheet Format) : 마이크로소프트 엑셀 파일포맷을 읽고 쓰는 컴포넌트로서 엑셀 97버전부터 현재까지 지원한다.
XSSF(XML SpreadSheet Format) : 마이크로소프트 엑셀 2007부터 지원하는 오피스 오픈 XML 파일 포맷인 *.xlsx 파일을 읽고 쓰는 컴포넌트이다.
HPSF(Horrible Property Set Format) : 오피스 파일의 문서요약 정보를 읽는데 사용되는 컴포넌트이다.
HWPF(Horrible Word Processor Format) : 마이크로소프트 워드 97(*.doc) 파일을 읽고 쓰는데 사용되는 컴포넌트이다. 아직까지는 개발 초기단계이다.
HSLF(Horrible Slid Layout Format) : 마이크로소프트 파워포인트 파일을 읽고 쓰는데 사용되는 컴포넌트이다.
HDGF(Horrible DiaGram Format) : 마이크로소프트 비지오 파일을 읽는데 사용하는 컴포넌트이다.
HPBF(Horrible PuBlisher Format) : 마이크로소프트 퍼블리셔 파일을 다루는데 사용되는 컴포넌트이다.
HSMF(Horrible PuBlisher Format) : 마이크로소프트 아웃룩에서 사용되는 *.msg 파일을 다루는데 사용되는 컴포넌트이다.
DDF(Dreadful Drawing Format) : 마이크로소프트 오피스에서 사용되는 이미지 파일을 읽어오는데 사용하는 컴포넌트이다.
HSSF 컴포넌트가 가장 안정적이고 많은 기능을 지원하며 다른 컴포넌트들은 사용은 가능하나 아직까지는 개발 단계이다.

API 주소 https://poi.apache.org/apidocs/
   

----------------사용 예제---------------------
    //테이블 데이터 클래스
    public struct LocalizingT
    {
        public int index;
        public string kor;
        public string eng;
    }

    public List<LocalizingT> m_LocalizingData = new List<LocalizingT>();

    //로컬라이징 테이블 데이터 
    #region Localizing
    public void Load_LocalizingTable()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/LocalizingTable", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                LocalizingT item = new LocalizingT();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                if (stral.Length > 1)
                {
                    item.kor = stral[cnt]; ++cnt;
                }
                if (stral.Length > 2)
                {
                    item.eng = stral[cnt]; ++cnt;
                }

                m_LocalizingData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }
    }

*/

public class ExcelParser : Editor
{

    const string m_Default_InputFolder = "/ExcelTable";
    const string m_Default_OutputFolder = "/Resources/Table";

    [MenuItem("Table/ExcelParser")]
    static void ExcelParser_Default()
    {
        StringBuilder TotalData = new StringBuilder();

        //시작
        DirectoryInfo D_Info = new DirectoryInfo(string.Format("{0}{1}", Application.dataPath, m_Default_InputFolder));

        FileInfo[] F_Info = D_Info.GetFiles("*.xlsx"); //*.xls로 찾아도 xlsx까지 가져온다.

        foreach (FileInfo info in F_Info)
        {
            ISheet sheet;

            if (info.Extension.Equals(".xls"))
            {
                HSSFWorkbook hssfwb;
                using (FileStream file = new FileStream(info.FullName, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new HSSFWorkbook(file);
                }

                for (int k = 0; k < hssfwb.NumberOfSheets; k++)
                {
                    sheet = hssfwb.GetSheetAt(k);
                    MakeSheetData(TotalData, info, sheet);
                }
            }
            else //xlsx
            {
                XSSFWorkbook hssfwb;
                using (FileStream file = new FileStream(info.FullName, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new XSSFWorkbook(file);
                }


                for (int k = 0; k < hssfwb.NumberOfSheets; k++)
                {
                    sheet = hssfwb.GetSheetAt(k);
                    MakeSheetData(TotalData, info, sheet);
                }
            }

        }

        AssetDatabase.Refresh();
        Debug.Log("Excel Parsing Done");
    }

    private static void MakeSheetData(StringBuilder TotalData, FileInfo info, ISheet sheet)
    {
        //초기화
        TotalData.Remove(0, TotalData.Length);

        IRow row;
        string str = "";

        //row = sheet.GetRow(0);        
        //if (row == null) return;
        //int CellMaxCount = row.Cells.Count;
        //int CellCount = 0;

        for (int i = 1; i <= sheet.LastRowNum; i++)  //스킵하고 싶은 라인까지 i의 초기값을 올린다.
        {
            row = sheet.GetRow(i);

            if (row == null)
            {
                Debug.LogError(sheet.SheetName + "-> " + i + "번째 줄부터 ~ " + sheet.LastRowNum + "번째 줄까지 값이 없는 빈공간이 존재합니다. 삭제해 주세요!!!");
                return;
            }

            //row.GetCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK);   //null과 Blank는 다른 개념인데 원하는 값으로 리턴해주는 방법??
            if (row.GetCell(0) == null)   //줄에 첫번째 셀부터 null
                continue;

            foreach (ICell cell in row)
            {
                str = "";

                switch (cell.CellType)
                {
                    case CellType.String:
                        str = cell.StringCellValue;
                        break;
                    case CellType.Numeric:
                        //숫자포맷이 날자일경우.. 게임에서는 사용하지 않을테니 주석처리.
                        //if (HSSFDateUtil.IsCellDateFormatted(cell))
                        //    str = cell.DateCellValue.ToString();
                        //else
                        str = cell.NumericCellValue.ToString();
                        break;
                    case CellType.Formula:
                        //공식을 가져올수 있지만 게임상에서는 결과값만을 사용하므로 결과의 타입을 한번 더 체크한다.
                        switch (cell.CachedFormulaResultType)
                        {
                            case CellType.String:
                                str = cell.StringCellValue;
                                break;
                            case CellType.Numeric:
                                str = cell.NumericCellValue.ToString();
                                break;
                            default:
                                str = "Formula(공식)의 결과값이 처리하지 않은 타입의 데이터입니다. Type : " + cell.CachedFormulaResultType.ToString();
                                Debug.LogError(str);
                                break;
                        }
                        break;
                    case CellType.Blank:

                        break;
                    default:
                        str = "테이블의 Cell들 중에 Parcer에서 처리하지 않은 타입의 데이타가 발생했습니다. Type : " + cell.CellType.ToString();
                        Debug.LogError(str);
                        break;
                }
                TotalData.Append(str);
                TotalData.Append("|"); //shift + \
            }

            //CellCount = row.Cells.Count;
            //if(CellCount < CellMaxCount)    //값이 없는 셀이 뒤쪽에 붙어있을경우 최대 갯수보다 적으므로 '|'를 채워준다.
            //{
            //    int cnt = (CellMaxCount - CellCount) * 2;
            //    for(int k = 0; k< cnt; k++)
            //        TotalData.Append("|"); //shift + \
            //}

            TotalData.Append("\n");

            string OutputPath = Application.dataPath + m_Default_OutputFolder + "/" + Path.GetFileNameWithoutExtension(sheet.SheetName) + ".txt";
            File.WriteAllText(OutputPath, TotalData.ToString());
        }//
    }
}


