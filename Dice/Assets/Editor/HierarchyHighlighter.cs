﻿//HierarchyHighlighter.cs - Put this script under Editor folder. 
//If you do not have any Editor folder in Unity, create one.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;


[InitializeOnLoad]
public class HierarchyHighlighter : Editor
{
    private static Vector2 offset = new Vector2(0, 2);

    static HierarchyHighlighter()
    {
        //EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemColorBox;
    }

    private static void HierarchyWindowItemColorBox(int selectionID, Rect selectionRect)
    {
        Object o = EditorUtility.InstanceIDToObject(selectionID);
        if (o == null)
            return;
        GameObject obj = o as GameObject;

        if (Event.current.type == EventType.Repaint)
        {
            //글자색 바꾸기
            Rect offsetRect = new Rect(selectionRect.position + offset, selectionRect.size);
            //EditorGUI.DrawRect(selectionRect, backgroundColor); //나중에 폰트스타일 바꾸면 이걸로 배경 다시 칠해줘야 뒷 글자가 안보임
            EditorGUI.LabelField(offsetRect, obj.name, new GUIStyle()
            {
                normal = new GUIStyleState() { textColor = GetColor(obj) },
                fontStyle = FontStyle.Normal
            });

            //GUI.contentColor = GetColor(obj);
            //배경색 바꾸기
            //GUI.backgroundColor = GetColor(obj);
            //GUI.Box(selectionRect, "");
            //GUI.backgroundColor = Color.white;
            //EditorApplication.RepaintHierarchyWindow();
        }
    }

    static Color GetColor(GameObject obj)
    {
        float baseAlpha = 1;
        Color transparency = new Color(1, 1, 1, 0);

        if (obj == null)
            return transparency;

        Transform tr = obj.transform;
        UIRoot[] temp = tr.GetComponentsInChildren<UIRoot>(true);

        if (obj.tag == "Bone")
        {
            if (obj.activeInHierarchy)
                return new Color(0, 1, 0, baseAlpha);
            else
                return transparency;
        }

        if (obj.tag == "Skin")
        {
            if (obj.activeInHierarchy)
                return new Color(1, 1, 0, baseAlpha);
            else
                return transparency;
        }

        return transparency;
    }
}