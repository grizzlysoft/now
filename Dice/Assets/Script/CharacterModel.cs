﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterModel : MonoBehaviour 
{
    public enum State
    {
        Idle,
        Attack,
        Skill,
        Hit,
        Deadly
    }

    public int depth = 100;

    string characterName;
    Animation anim;
    UIPanel panel;
    NGUI_ColorSetter colorSetter;

    void Awake()
    {
        characterName = this.gameObject.name.Split('_')[1].Replace("(Clone)","");

        anim = this.GetComponent<Animation>();
        panel = this.GetComponent<UIPanel>();
        colorSetter = this.GetComponentInChildren<NGUI_ColorSetter>();


        SetAnim(State.Idle);
        panel.depth = depth;
        colorSetter.Init(characterName);
    }

    public void SetAnim(State _state)
    {
        anim.Play(string.Format("{0}@{1}", characterName, _state.ToString()));
    }

}
