﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour 
{
    Animation anim;

    [SerializeField] UISprite sp_Dice;
    [SerializeField] UILabel lb_Dice;

    public int value { get; private set; }

    private void Awake()
    {
        anim = this.GetComponent<Animation>();
        Hide();
    }

    public void RollDice(int _diceSkin, int _diceType)
    {
        this.gameObject.SetActive(true);

        //주사위 굴린 결과 저장
        value = Random.Range(1, _diceType + 1);

        sp_Dice.spriteName = string.Format("{0}_d{1}", _diceSkin , _diceType);
        lb_Dice.text = value.ToString();
        lb_Dice.color = GetTextColor((DiceType)_diceType);

        anim.Play("Dice@Roll");
    }

    Color GetTextColor(DiceType _diceType)
    {
        switch (_diceType)
        {
            case DiceType.d4:
                return UtilityTools.HexToColor("FF8080FF");
            case DiceType.d6:
                return UtilityTools.HexToColor("FFFFFFFF");
            case DiceType.d8:
                return UtilityTools.HexToColor("FFD870FF");
            case DiceType.d10:
                return UtilityTools.HexToColor("A8FF8DFF");
            case DiceType.d12:
                return UtilityTools.HexToColor("81EAFFFF");
            case DiceType.d20:
                return UtilityTools.HexToColor("FF7EE1FF");
        }

        return Color.black;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

}
