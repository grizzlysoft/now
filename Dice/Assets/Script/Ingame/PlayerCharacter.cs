﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerCharacter : MonoBehaviour 
{
    //데이터
    public Data_Manager.CharacterSaveData data;

    //상태이상 리스트
    List<Debuff> li_Debuff = new List<Debuff>();

    //모델
    CharacterModel characterModel;

    //주사위
    [SerializeField] UIGrid gr_Dice;
    List<Dice> li_Dice = new List<Dice>();

    //UI

    
    public void Init(int _index)
    {
        data = Data_Manager.Get.SelectCharacter_GetSelectCharacter(_index);

        GameObject go = Resources.Load(string.Format("Prefab/{0}", data.tableData.prefabName)) as GameObject;
        GameObject temp = Instantiate(go, this.transform);
        characterModel = temp.GetComponent<CharacterModel>();

        //주사위 생성
        for(int i=0; i<5; i++)
        {
            go = Resources.Load("Prefab/Dice") as GameObject;
            temp = Instantiate(go, gr_Dice.transform);
            li_Dice.Add(temp.GetComponent<Dice>());
        }
        gr_Dice.Reposition();

    }

    public void GetDebuff(Debuff _get)
    {
        li_Debuff.Add(_get);
    }

    public IEnumerator RollDice()
    {
        for (int i=0; i<data.USE_DICE; i++)
        {
            li_Dice[i].RollDice(0, data.tableData.dice[i]);
            gr_Dice.Reposition();
            yield return new WaitForSeconds(0.2f);
        }

        //주사위 애니메이션 끝나는거 기다려줌 - 애니메이션 시간만큼
        yield return new WaitForSeconds(2.2f);
        Battle_Manager.Get.SetTotalDamage(GetTotalValue());
    }

    public int GetTotalValue()
    {
        int temp = 0;
        for (int i = 0; i < data.USE_DICE; i++)
            temp += li_Dice[i].value;

        return temp;
    }

    public void Dice_AllHide()
    {
        for (int i = 0; i < li_Dice.Count; i++)
            li_Dice[i].Hide();
    }
}
