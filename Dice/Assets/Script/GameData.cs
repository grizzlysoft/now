﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData 
{
    #region AccountData

    public static int Gold { get; private set; }
    public static Del_Int Gold_Del;

    public static int Cash { get; private set; }
    public static Del_Int Cash_Del;

    public static int AdventureKey { get; private set; }
    public static Del_Int AdventureKey_Del;

    public static int DungeonKey { get; private set; }
    public static Del_Int DungeonKey_Del;
    
    #region AccountFuction
    public static void AddGold(int add_)
    {
        Gold += add_;
        Gold_Del(Gold);
    }

    public static bool UseGold(int use_)
    {
        if (Gold < use_)
            return false;
        else
        {
            Gold -= use_;
            Gold_Del(Gold);
            return true;
        }
    }

    public static void AddCash(int add_)
    {
        Cash += add_;
        Cash_Del(Cash);
    }

    public static bool UseCash(int use_)
    {
        if (Cash < use_)
            return false;
        else
        {
            Cash -= use_;
            Cash_Del(Cash);
            return true;
        }
    }

    public static void AddAdventureKey(int add_)
    {
        AdventureKey += add_;
        AdventureKey_Del(add_);
    }

    public static bool UseAdventureKey(int use_)
    {
        if (AdventureKey < use_)
            return false;
        else
        {
            AdventureKey -= use_;
            AdventureKey_Del(AdventureKey);
            return true;
        }
    }

    public static void AddDungeonKey(int add_)
    {
        DungeonKey += add_;
        DungeonKey_Del(DungeonKey);
    }

    public static bool UseDungeonKey(int use_)
    {
        if (DungeonKey < use_)
            return false;
        else
        {
            DungeonKey -= use_;
            DungeonKey_Del(DungeonKey);
            return true;
        }
    }
    #endregion

    #endregion

    #region InventoryData

    public static List<Inven_Character> CharacterList = new List<Inven_Character>();
    public static List<Inven_Item> ItemList = new List<Inven_Item>();

    #region InventoryFunction

    public static void AddInvenItem(Inven_Item item_)
    {
        Inven_Item finditem = ItemList.Find(r => r.ID == item_.ID);
        if(finditem != null)
        {
            finditem.Count += item_.Count;
        }
        else
        {
            ItemList.Add(item_);
        }

        //갱신 해줄거 있으면 여기서
    }

    public static bool UseInvenItem(int id_, int count_)
    {
        Inven_Item finditem = ItemList.Find(r => r.ID == id_);

        if (finditem == null) //해당 아이템 없음
            return false;
        else if (finditem.Count < count_) //아이템 모지람
            return false;
        else
        {
            finditem.Count -= count_;

            if (finditem.Count <= 0) //아이템 갯수0이면 삭제
                ItemList.Remove(finditem);

            return true;
        }

        //갱신처리
    }
    #endregion

    #endregion

    #region AchievementData
    static int[] Achievment_Value = new int[(int)AchievementType.Max];
    static bool[] Achievement_Clear;
    static int[] NowAchievementID = new int[(int)AchievementType.Max];
    
    //업적은 불러오기 전에 초기화 해준다
    static void AchievementInit()
    {
        Achievement_Clear = new bool[Table_Manager.Get.li_Achievement.Count];

        for (int i = 0; i < Achievement_Clear.Length; ++i)
            Achievement_Clear[i] = false;

        for (int i = 0; i < (int)AchievementType.Max; ++i)
        {
            NowAchievementID[i] = Table_Manager.Get.li_Achievement.Find(r => r.Type == (AchievementType)i && r.First == 1).ID;
            Achievment_Value[i] = 0;
        }
    }

    public static void AddAchievementValue(AchievementType type_, int value_)
    {
        Table_Manager.AchievementData sample = Table_Manager.Get.li_Achievement.Find(r => r.Type == type_);

        Achievment_Value[(int)type_] += value_;

        if (sample.MaxValue < Achievment_Value[(int)type_])
        {
            Achievment_Value[(int)type_] = sample.MaxValue;
        }
    }

    public static void ClearAchievement(AchievementType type_)
    {
        Table_Manager.AchievementData CheckData = Table_Manager.Get.li_Achievement.Find(r => r.ID == NowAchievementID[(int)type_]);

        if(Achievment_Value[(int)type_] >= CheckData.ClearValue && Achievement_Clear[CheckData.ID] == false)
        {
            //현재 업적 교체
            NowAchievementID[(int)type_] = CheckData.NextID;
            //업적 클리어 처리
            Achievement_Clear[CheckData.ID] = true;

            //보상처리
            PaymentManager.Get.PayPrice(CheckData.RewardID);

            //ui갱신
        }
        else
        {
            //조건이 안되었거나 이미 클리어한 업적이다
        }
    }
    #endregion

    #region Dungeon_InfoData
    #endregion
}
