﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main_Manager : Singleton<Main_Manager>
{
    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        Table_Manager.Get.LoadTable();
        Data_Manager.Get.LoadAllData();
    }

    public void Start()
    {
        //FB_Manager.Get.InitFBDB();
        //Purchasing.Get.InitPurchasing();
    }

    public void ChangeSceneBattle()
    {
        SceneManager.LoadScene("BattleScene");
    }
       
    public void ChangeSceneBase()
    {
        SceneManager.LoadScene("BaseScene");
    }

    public void Update()
    {
        
    }

    private void OnApplicationPause(bool pause)
    {
        
    }

    private void OnApplicationQuit()
    {
        
    }
}
