﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine.SocialPlatforms;
using Google;
using System.Threading.Tasks;

public class GPGS_Manager : Singleton<GPGS_Manager>
{
    

    public void Init()
    {
#if !UNITY_EDITOR
         PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .EnableSavedGames()
        .Build();
        //GPGS 시작.
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        GPGS_Login();
#endif
    }

    public void GPGS_Login()
    {
        if (Social.localUser.authenticated == true)
        {
            Debug.Log("로그인했다");
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("GPGS 로그인 성공");
                }
                else
                {
                    // 로그인 실패
                    Debug.Log("Login failed for some reason");
                }
            });
        }
    }

    //gpgs 관련
    //인증여부 확인
    public bool CheckLogin()
    {
        return Social.localUser.authenticated;
    }

    /// <summary> 리더보드 UI show </summary>
    public void ShowLeaderBoard()
    {
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.

            return;
        }
        Social.Active.ShowLeaderboardUI();
    }

    /// <summary> 업적 UI show </summary>
    public void ShowAchievement()
    {
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.

            return;
        }
        Social.ShowAchievementsUI();
    }

    /// <summary> 리더보드 점수 업로드 </summary>
    public void ReportScore(string _id, int _value)
    {
#if !UNITY_EDITOR
        Social.ReportScore(_value, _id, (bool success) =>
        {
            if (success) Debug.Log("ReportScore Success");

            else Debug.Log("ReportScore Fail");
        });
#endif
    }

    public void UnlockAchievement(string _id)
    {
        PlayGamesPlatform.Instance.UnlockAchievement(_id, (bool success) =>
        {
            if (success) Debug.Log("UnlockAchievement Success");

            else Debug.Log("UnlockAchievement Fail");
        });
    }
}
