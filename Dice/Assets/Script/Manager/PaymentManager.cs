﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaymentManager : Singleton<PaymentManager>
{
    public void BuyPrice(Table_Manager.ShopData data_)
    {
        switch (data_.PayType)
        {
            case PayType.CASH:
                Purchasing.Get.Buy(data_.StoreID);
                break;
            case PayType.ADS:
                Ads_Manager.Get.ShowAd_UnityAds(ShowAdType.Max, data_.RewardID[0]);
                break;
        }
    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">리워드 시트 id</param>
    public void PayPrice(int id_)
    {
        Table_Manager.RewardData data = Table_Manager.Get.li_RewardData.Find(r => r.ID == id_);

        switch (data.Type)
        {
            case RewardType.Dia:
                Debug.Log("Dia " + data.Count + "지급");
                break;
            case RewardType.Gold:
                break;
        }
    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">스토어 id</param>
    public void PayPrice(string id_)
    {
        Table_Manager.ShopData s = Table_Manager.Get.li_ShopData.Find(r => r.StoreID == id_);
        for(int i = 0; i < s.RewardID.Count; ++i)
            PayPrice(s.RewardID[i]);
    }

}
