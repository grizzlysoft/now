﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data_Manager : Singleton<Data_Manager>
{
    public class CharacterSaveData
    {
        //Save
        public int id { get; private set; }
        public int Level { get; private set; }
        public int EXP { get; private set; }

        //NonSave
        public Table_Manager.CharacterData tableData { get; private set; }
        public int POW { get; private set; }
        public int CON { get; private set; }
        public int WIS { get; private set; }

        public int HP { get; private set; }
        public int MP { get; private set; }
        
        public int USE_DICE { get; private set; }

        public CharacterSaveData(int _id , int _Level = 1, int _EXP = 0)
        {
            id = _id;
            Level = _Level;
            EXP = _EXP;
            //로컬 테이블도 가져와줌
            tableData = Table_Manager.Get.li_CharacterData.Find(r => r.id == _id);
            SetCurrentLevelData();
        }
        void SetCurrentLevelData()
        {
            POW = tableData.POW + (tableData.POW_Increase * (Level - 1));
            CON = tableData.CON + (tableData.CON_Increase * (Level - 1));
            WIS = tableData.WIS + (tableData.WIS_Increase * (Level - 1));
            USE_DICE = (Level + 1) / 2;

            HP = CON * 10;
            MP = WIS * 5;
        }
        public bool AddExp(int _Exp)
        {
            //만렙 체크
            if (tableData.MaxLevel <= Level)
                return false;

            //경험치 추가
            EXP += _Exp;

            //레벨업 체크 
            if (Table_Manager.Get.li_LevelData[Level].nextExp <= EXP)
            {
                Level++;
                EXP = 0;
                SetCurrentLevelData();
                return true;
            }

            return false;
        }
        
    }
    /// <summary> 내가 보유한 캐릭터 정보 </summary>
    List<CharacterSaveData> li_Character = new List<CharacterSaveData>();
    public void Character_Add(int _id)
    {
        CharacterSaveData temp = new CharacterSaveData(_id);
        li_Character.Add(temp);
    }
    public bool Character_AddEXP(int _id , int _Exp)
    {
        CharacterSaveData Find = li_Character.Find(r => r.id == _id);
        if (Find == null)
        {
            Debug.LogError("Find 실패 : 없는 캐릭터");
            return false;
        }

        return Find.AddExp(_Exp);
    }
    public CharacterSaveData Character_GetInfo(int _id)
    {
        CharacterSaveData Find = li_Character.Find(r => r.id == _id);
        return Find;
    }

    /// <summary> 파티 구성 캐릭터 정보(전투에 쓸 선택한 캐릭터들) </summary>
    List<int> li_SelectCharacter = new List<int>();
    /// <summary> _index 번째 캐릭터 정보를 가져온다 </summary>
    public CharacterSaveData SelectCharacter_GetSelectCharacter(int _index)
    {
        return Character_GetInfo(li_SelectCharacter[_index]);
    }
    public void SelectCharacter_Add(int _id)
    {
        if (SelectCharacter_Check() == false)
            li_SelectCharacter.Add(_id);
        else
            Debug.Log("모든 캐릭터를 선택함");
    }
    public void SelectCharacter_Remove(int _id)
    {
        li_SelectCharacter.Remove(_id);

    }
    public bool SelectCharacter_Check()
    {
        if (li_SelectCharacter.Count < 5)
            return false;

        return true;
    }

    public void LoadAllData()
    {
        //기본 캐릭터 5마리 넣어줌
        for (int i = 0; i < 5; i++)
        {
            Character_Add(i);
            SelectCharacter_Add(i); //<<기본 5명 파티로 선택해둠
        }
    }
}
