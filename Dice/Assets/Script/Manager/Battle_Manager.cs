﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle_Manager : Singleton<Battle_Manager> 
{
    struct IngameData
    {
        public int TotalDmg;
        public int HP;
        public int MAX_HP;
        public int MP;
        public int MAX_MP;
        public int Regen_HP;
        public int Regen_MP;

        public void ResetData()
        {
            TotalDmg = 0;
            HP = 0;
            MAX_HP = 0;
            MP = 0;
            MAX_MP = 0;
            Regen_HP = 0;
            Regen_MP = 0;
        }
    }

    IngameData PlayerData = new IngameData();
    IngameData EnemyData = new IngameData();

    //인게임 데이터

    //인게임 UI
    [SerializeField] UISprite sp_PlayerHP;
    [SerializeField] UISprite sp_PlayerMP;
    [SerializeField] UISprite sp_EnemyHP;
    [SerializeField] UISprite sp_EnemyMP;

    [SerializeField] UILabel lb_Player_TotalAttack;
    [SerializeField] UILabel lb_Enemy_TotalAttack;

    [SerializeField] GameObject go_UpEffect;

    //각 캐릭터 정보
    public List<PlayerCharacter> li_Character = new List<PlayerCharacter>();

    //추가 변수
    bool isOnClick_Roll = false;
    

    void ResetAllParameta()
    {
        isOnClick_Roll = false;

        PlayerData.ResetData();
        EnemyData.ResetData();
    }

    private void Start()
    {
        ResetAllParameta();

        //캐릭터 생성 MAX_PARTY
        for (int i=0; i<li_Character.Count; i++)
        {
            li_Character[i].Init(i);
            //캐릭터 정보 셋팅
            PlayerData.MAX_HP += li_Character[i].data.HP;
            PlayerData.MAX_MP += li_Character[i].data.MP;
            PlayerData.HP = PlayerData.MAX_HP;
            PlayerData.MP = PlayerData.MAX_MP;
            PlayerData.TotalDmg += li_Character[i].data.POW;
            PlayerData.Regen_HP += li_Character[i].data.CON;
            PlayerData.Regen_MP += li_Character[i].data.WIS;
        }
        
        Refresh_StateBar();
        Refresh_TotalDmg();

        StartCoroutine(StartBattle());
    }

    private void Update()
    {
    }

    IEnumerator StartBattle()
    {
        //시작 연출
        yield return null;
        
        while (true)
        {
            //이거 테스트 용임 나중에 수정하셈
            if (isOnClick_Roll == true)
            {
                isOnClick_Roll = false;
                StartCoroutine(RollDice_AllCharacter());
            }
            yield return null;
        }


    }

    IEnumerator RollDice_AllCharacter()
    {
        for (int i = 0; i < li_Character.Count; i++)
        {
            StartCoroutine(li_Character[i].RollDice());
            yield return new WaitForSeconds(0.6f);
        }

        yield return null;
    }

    public void SetTotalDamage(int _value)
    {
        PlayerData.TotalDmg += _value;
        lb_Player_TotalAttack.text = PlayerData.TotalDmg.ToString();
        GameObject temp = Instantiate(go_UpEffect, lb_Player_TotalAttack.transform) as GameObject;
        temp.GetComponent<UpLabelEffect>().Show(Vector3.zero, string.Format("+{0}",_value.ToString()) );
    }

    public void Hide_Dice()
    {
        for(int i=0; i<li_Character.Count; i++)
        {
            li_Character[i].Dice_AllHide();
        }

        ResetAllParameta();
        Refresh_StateBar();
        Refresh_TotalDmg();
    }

    #region UI
    //갱신 함수
    void Refresh_StateBar()
    {
        sp_PlayerHP.fillAmount = (float)PlayerData.HP / PlayerData.MAX_HP;
        sp_PlayerMP.fillAmount = (float)PlayerData.MP / PlayerData.MAX_MP;

        sp_EnemyHP.fillAmount = (float)EnemyData.HP / EnemyData.MAX_HP;
        sp_EnemyMP.fillAmount = (float)EnemyData.MP / EnemyData.MAX_MP;
    }
    void Refresh_TotalDmg()
    {
        lb_Player_TotalAttack.text = PlayerData.TotalDmg.ToString();
        lb_Enemy_TotalAttack.text = EnemyData.TotalDmg.ToString();
    }

    //버튼 함수 
    public void OnClick_PauseBattle()
    {
        Hide_Dice();
    }
    public void OnClick_Roll()
    {
        isOnClick_Roll = true;
    }
    #endregion
}
