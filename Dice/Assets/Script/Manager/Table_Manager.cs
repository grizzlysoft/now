﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table_Manager : Singleton<Table_Manager>
{
    public void LoadTable()
    {
        TableLoad("Character", LoadTable_CharacterData);
        TableLoad("Level", LoadTable_LevelData);
        TableLoad("Shop", LoadTable_ShopData);
        TableLoad("Reward", LoadTable_RewardData);
    }

    ////////////테이블 파싱
    delegate void Parse(string[] st);
    void TableLoad(string Name, Parse del)
    {
        TextAsset txObj = Resources.Load(string.Format("Table/{0}", Name), typeof(TextAsset)) as TextAsset;
        string[] strArray = txObj.text.Split('\n');
        for (int i = 0; i < strArray.Length; i++)
        {
            if (strArray[i] == "" || strArray[i][0] == '|') break;

            strArray[i] = strArray[i].Replace("\\n", "\n");
            string[] stral = strArray[i].Split('|');
            //Debug.Log(Name + " Parse : " + i);
            del(stral);
        }
    }
    static List<int> GetListData(string _ar)
    {
        List<int> temp = new List<int>();
        string[] ar = _ar.Split(',');

        if (ar[0] != "")
        {
            for (int i = 0; i < ar.Length; i++)
            {
                temp.Add(int.Parse(ar[i]));
            }
        }

        return temp;
    }

    #region Character
    public class CharacterData
    {
        public readonly int id;
        public readonly string name;
        public readonly RegionType regionType;
        public readonly ClassType classType;
        public readonly int MaxLevel;
        public readonly int POW;
        public readonly int POW_Increase;
        public readonly int CON;
        public readonly int CON_Increase;
        public readonly int WIS;
        public readonly int WIS_Increase;
        public readonly List<int> dice = new List<int>();
        public readonly int skill_1;
        public readonly int skill_2;
        public readonly string prefabName;

        public CharacterData(string[] stral)
        {
            int cnt = 0;
            id = int.Parse(stral[cnt]); cnt++;
            name = stral[cnt]; cnt++;
            regionType = (RegionType)int.Parse(stral[cnt]); cnt++;
            classType = (ClassType)int.Parse(stral[cnt]); cnt++;
            cnt++;//total 

            MaxLevel = int.Parse(stral[cnt]); cnt++;
            POW = int.Parse(stral[cnt]); cnt++;
            POW_Increase = int.Parse(stral[cnt]); cnt++;
            CON = int.Parse(stral[cnt]); cnt++;
            CON_Increase = int.Parse(stral[cnt]); cnt++;
            WIS = int.Parse(stral[cnt]); cnt++;
            WIS_Increase = int.Parse(stral[cnt]); cnt++;

            /*
             string[] ar = stral[cnt].Split(','); ++cnt;
            for (int i = 0; i < ar.Length; i++)
                dice.Add(int.Parse(ar[i]));
             */

            dice.Add(int.Parse(stral[cnt])); ; cnt++;
            dice.Add(int.Parse(stral[cnt])); ; cnt++;
            dice.Add(int.Parse(stral[cnt])); ; cnt++;
            dice.Add(int.Parse(stral[cnt])); ; cnt++;
            dice.Add(int.Parse(stral[cnt])); ; cnt++;

            skill_1 = int.Parse(stral[cnt]); cnt++;
            skill_2 = int.Parse(stral[cnt]); cnt++;
            prefabName = stral[cnt]; cnt++;
        }
    }
    public List<CharacterData> li_CharacterData = new List<CharacterData>();
    void LoadTable_CharacterData(string[] stral)
    {
        li_CharacterData.Add(new CharacterData(stral));
    }
    #endregion
    #region Level
    public class LevelData
    {
        public readonly int id;
        public readonly int nextExp;

        public LevelData(string[] stral)
        {
            int cnt = 0;
            id = int.Parse(stral[cnt]); cnt++;
            nextExp = int.Parse(stral[cnt]); cnt++;
        }
    }

    public List<LevelData> li_LevelData = new List<LevelData>();
    void LoadTable_LevelData(string[] stral)
    {
        li_LevelData.Add(new LevelData(stral));
    }
    #endregion

    #region Shop
    public class ShopData
    {
        public readonly int ID;
        public readonly PayType PayType;
        public readonly string Cost_Test;
        public readonly string Name;
        public readonly string StoreID;
        public readonly List<int> RewardID;
        public readonly ShopTap Tap;
        public readonly int Oder;
        public readonly string Res;

        public ShopData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            PayType = (PayType)int.Parse(stral[cnt]); cnt++;
            Cost_Test = stral[cnt]; cnt++;
            Name = stral[cnt]; cnt++;
            StoreID = stral[cnt]; cnt++;
            RewardID = GetListData(stral[cnt]); cnt++;
            Tap = (ShopTap)int.Parse(stral[cnt]); cnt++;
            Oder = int.Parse(stral[cnt]); cnt++;
            Res = stral[cnt]; cnt++;
        }
    }
    public List<ShopData> li_ShopData = new List<ShopData>();
    void LoadTable_ShopData(string[] stral)
    {
        li_ShopData.Add(new ShopData(stral));
    }

    public List<ShopData> GetShopData_byTap(ShopTap tap_)
    {
        List<ShopData> list = new List<ShopData>();

        for(int i = 0; i < li_ShopData.Count; ++i)
        {
            if (tap_ == li_ShopData[i].Tap)
                list.Add(li_ShopData[i]);
        }

        return list;
    }
    #endregion

    #region Reward
    public class RewardData
    {
        public readonly int ID;
        public readonly RewardType Type;
        public readonly string Name;
        public readonly int Count;

        public RewardData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            Type = (RewardType)int.Parse(stral[cnt]); cnt++;
            Name = stral[cnt]; cnt++;
            Count = int.Parse(stral[cnt]); cnt++;
        }
    }

    public List<RewardData> li_RewardData = new List<RewardData>();
    void LoadTable_RewardData(string[] stral)
    {
        li_RewardData.Add(new RewardData(stral));
    }
    #endregion

    #region Dungeon
    public class Dungeon_Main
    {

    }

    public List<Dungeon_Main> li_DungeonData = new List<Dungeon_Main>();
    #endregion

    #region Achievement
    public class AchievementData
    {
        public readonly int ID;
        public readonly AchievementType Type;
        public readonly string Name;
        public readonly int ClearValue;
        public readonly int MaxValue;
        public readonly int NextID;
        public readonly int First;
        public readonly int RewardID;
        public readonly string GpgsID;

        public AchievementData(string[] stral)
        {

        }
    }

    public List<AchievementData> li_Achievement = new List<AchievementData>();

    void LoadTable_Achievement(string[] stral)
    {
        li_Achievement.Add(new AchievementData(stral));
    }
    #endregion
}

