﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(AudioListener))]
[RequireComponent(typeof(AudioSource))]
public class Sound_Manager : Singleton<Sound_Manager>
{
    public string currentBGM = "";
    AudioSource BGM_Source;
    
    //모든 오디오 정보 파일
    Dictionary<string, AudioClip> dic_Audio = new Dictionary<string, AudioClip>();
    //이펙트 사운드 정보
    Dictionary<string, AudioSource> dic_Effect = new Dictionary<string, AudioSource>();

    public float BGM_Volume;
    public float Effect_Volume;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        //사운드 폴더 내 오디오 파일 접근
        dic_Audio.Clear();
        AudioClip[] soundFile =  Resources.LoadAll<AudioClip>("Sound");
        for (int i = 0; i < soundFile.Length; i++)
        {
            dic_Audio.Add(soundFile[i].name, soundFile[i]);
        }

        //BGM 셋팅
        BGM_Source = this.GetComponent<AudioSource>();
        BGM_Source.loop = true;
        BGM_Source.playOnAwake = false;

        //Effect 셋팅
        GameObject Effect_Preset = Resources.Load<GameObject>("Prefab/Effect_Preset");
        if (Effect_Preset == null)
        {
            Debug.LogError("이펙트 프리셋 없음");
            return;
        }

        GameObject tempEffect;
        AudioSource tempScp;
        for (int i = 0; i < soundFile.Length; i++)
        {
            if (soundFile[i].name.Contains("Effect") == true)
            {
                tempEffect = Instantiate(Effect_Preset, this.transform) as GameObject;
                tempScp = tempEffect.GetComponent<AudioSource>();
                tempEffect.name = soundFile[i].name;
                tempScp.clip = soundFile[i];
                dic_Effect.Add(soundFile[i].name, tempScp);
            }
        }

        BGM_Volume = PlayerPrefs.GetFloat("BGM_Volume", 1.0f);
        Effect_Volume = PlayerPrefs.GetFloat("Effect_Volume", 1.0f);
        SetVolume_BGM(BGM_Volume);
        SetVolume_Effect(Effect_Volume);
    }

    //BGM 소리 조절
    public void SetVolume_BGM(float _value)
    {
        BGM_Volume = _value;
        PlayerPrefs.SetFloat("BGM_Volume", BGM_Volume);

        BGM_Source.volume = BGM_Volume;
    }

    //Effect 소리 조절
    public void SetVolume_Effect(float _value)
    {
        Effect_Volume = _value;
        PlayerPrefs.SetFloat("Effect_Volume", Effect_Volume);
    }


    //BGM 변경
    public void PlayBGM(string _name)
    {
        if (currentBGM == _name)
            return;

        BGM_Source.clip = dic_Audio[_name];
        BGM_Source.time = 0;
        BGM_Source.Play();
        currentBGM = _name;
    }
    public void StopBGM()
    {
        BGM_Source.Stop();
    }

    //이펙트 사운드 1회 발동
    public void PlayEffect(string _name , bool isLoop = false)
    {
        if (dic_Effect.ContainsKey(_name) == false)
        {
            Debug.Log("없는 사운드이펙트");
            return;
        }

        dic_Effect[_name].loop = isLoop;
        dic_Effect[_name].time = 0;
        dic_Effect[_name].volume = Effect_Volume;
        dic_Effect[_name].Play();
    }
    public void StopEffect(string _name)
    {
        dic_Effect[_name].Stop();
    }
}
