﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UIPanel))]
public class Popup : MonoBehaviour
{
    TweenScale tw_Scale = null;
    TweenPosition tw_Position = null;
    
    /// <summary> 백버튼 영향 받을지 말지 여부 true == ESC로 닫힌다 </summary>
    protected bool isBackButtonClose = true;

    #region 팝업 상속 재정의 목록
    virtual public void Init()
    {
        this.gameObject.SetActive(true);

        Transform findTr = this.transform.Find("Base");
        if (findTr != null)
        {
            tw_Scale = findTr.GetComponent<TweenScale>();
            tw_Position = findTr.GetComponent<TweenPosition>();
        }
        
        this.gameObject.SetActive(false);
    }

    //팝업 형태
    virtual public void Show()
    {
        //재정의 할때 자식의 스크롤 뷰 등 UIPanel 뎁스를 반드시 바꿔주자
        this.GetComponent<UIPanel>().depth = Popup_Manager.Get.current_depth;

        Popup_Manager.Get.PushList(this);
        if (tw_Scale != null)
        {
            tw_Scale.ResetToBeginning();
            tw_Scale.enabled = true;
        }

        if (tw_Position != null)
        {
            tw_Position.ResetToBeginning();
            tw_Position.enabled = true;
        }
        
        this.gameObject.SetActive(true);
    }

    virtual public void Refresh()
    {
    }
    
    //소리없이 팝업 닫기?
    virtual public void PressBackButton()
    {
        if (isBackButtonClose == true)
            Hide();
    }
    #endregion

    //일반적으로 이걸 호출할 일은 없어야 한다,강제로 팝업이 닫힘. 버튼에는 OnClick_Hide를 사용
    public void Hide()
    {
        Popup_Manager.Get.PopList(gameObject);
        this.gameObject.SetActive(false);
    }

    //버튼 효과음 + 닫힘
    public void OnClick_Hide()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Hide();
    }
}

public class Popup_Manager : Singleton<Popup_Manager>
{
    const int defaultDepth = 200;
    public int current_depth = defaultDepth;
    

    public void Init()
    {

    }

    // 백버튼 관리
    List<Popup> li_Popup = new List<Popup>();
    public void PushList(Popup popup)
    {
        Popup temp = li_Popup.Find(r => r.gameObject == popup.gameObject);
        if (temp == null)
        {
            //뎁스는 열릴 때 마다 쌓여만 간다. -> 모든 팝업이 닫힌 이후에 뎁스를 기본값으로 초기화 한다.
            current_depth += 10;
            li_Popup.Add(popup);
        }
        else
            Debug.LogError("이미 열려있는 팝업!");
    }
    public void PopList(GameObject go)
    {
        if (li_Popup.Count > 0)
        {
            Popup temp = li_Popup.Find(r => r.gameObject == go);
            if (temp != null)
            {
                //마지막 팝업일 경우, 현재 뎁스를 초기화 해준다
                if (li_Popup.Count == 1)
                    current_depth = defaultDepth;

                li_Popup.Remove(temp);
            }
            else
                Debug.LogError("없는 팝업을 닫을려고 함!");
        }
    }

    void ShowLog()
    {
        Debug.Log("===================");
        for (int i = 0; i < li_Popup.Count; i++)
        {
            Debug.Log(i + li_Popup[i].name);
        }
        Debug.Log("===================");
    }

    // 백버튼 체크
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //리스트 아웃
            if (li_Popup.Count == 0)
            {
                 ExitGame();
            }
            else
            {
                li_Popup[li_Popup.Count - 1].PressBackButton();
            }
        }
    }

    void ExitGame()
    {

    }

    public void AllHide()
    {
        int count = li_Popup.Count;
        for (int i = 0; i < count; i++)
        {
            li_Popup[0].Hide();
        }
    }
}
