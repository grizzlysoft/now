﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using System;

public enum ShowAdType
{
    Max,
}

public class Ads_Manager : Singleton<Ads_Manager>
{
    private string
        androidGameId = "1727588";


    [HideInInspector]
    public bool AdMobReward = false;

    ShowAdType Type = ShowAdType.Max;
    int Reward_ID = -1;

    public void Ads_ManagerInit()
    {
        string gameId = null;

#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif

#if !UNITY_EDITOR
        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, false);
        }
#endif
    }

    public void ShowAd_UnityAds(ShowAdType type_, int rewardid_)
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            Reward_ID = rewardid_;
            Type = type_;
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("The ad was successfully shown.");

                if(-1 != Reward_ID)
                {
                    //보상지급
                    PaymentManager.Get.PayPrice(Reward_ID);
                }

                switch(Type)
                {
                    
                }

                //광고 보기 업적 카운트 ++
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        Type = ShowAdType.Max;
        Reward_ID = -1;

    }

    private void OnDestroy()
    {

    }

}

