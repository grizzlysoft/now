﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity;
using Firebase.Unity.Editor;
using Firebase;
using System;
using System.Threading.Tasks;
using Google;

public class FB_Manager : Singleton<FB_Manager>
{
    FirebaseAuth auth;
    Task UpLoadTask;
    Task<DataSnapshot> LoadTask;
    FirebaseUser FB_User;

    FBDB_UpLoadState UpLoadState = FBDB_UpLoadState.None;
    FBDB_LoadState LoadState = FBDB_LoadState.None;

    private void Awake()
    {
        
    }

    

    private void Start()
    {
        auth = FirebaseAuth.DefaultInstance;
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
        Firebase.Messaging.FirebaseMessaging.Subscribe("/topics/ProjectLF_Message");
        
    }

    public void Sigin()
    {
        Debug.Log("google sigin");
        GoogleSignIn.Configuration = new GoogleSignInConfiguration
        {
            RequestIdToken = true,
            // Copy this value from the google-service.json file.
            // oauth_client with type == 3
            WebClientId = "509429772436-lh7omdb02svcerreua48jvfu16jvqb4n.apps.googleusercontent.com"
        };

        Task<GoogleSignInUser> signIn = GoogleSignIn.DefaultInstance.SignIn();

        TaskCompletionSource<FirebaseUser> signInCompleted = new TaskCompletionSource<FirebaseUser>();
        signIn.ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.Log("google sign canceled");
                signInCompleted.SetCanceled();
            }
            else if (task.IsFaulted)
            {
                signInCompleted.SetException(task.Exception);
                Debug.Log("google sigin faill");
                Debug.Log("reseson = " + signInCompleted.Task.Exception);
            }
            else
            {

                Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(((Task<GoogleSignInUser>)task).Result.IdToken, null);
                auth.SignInWithCredentialAsync(credential).ContinueWith(authTask => {
                    if (authTask.IsCanceled)
                    {
                        signInCompleted.SetCanceled();
                    }
                    else if (authTask.IsFaulted)
                    {
                        signInCompleted.SetException(authTask.Exception);
                        Debug.Log("firebase sigin faill");
                        Debug.Log("reseson = " + signInCompleted.Task.Exception);
                    }
                    else
                    {
                        signInCompleted.SetResult(((Task<FirebaseUser>)authTask).Result);
                        FB_User = signInCompleted.Task.Result;
                        Debug.LogFormat("User signed in successfully: {0} ({1})",FB_User.DisplayName, FB_User.UserId);
                        CheckData();
                    }
                });
            }
        });
    }

    public void InitFBDB()
    {
#if !UNITY_EDITOR
        Sigin();
        //Debug.Log("FBDB INIT");
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://projectlf-19839882.firebaseio.com/");
        FirebaseApp.DefaultInstance.SetEditorP12FileName("ProjectLF-f5167fe953c2");
        FirebaseApp.DefaultInstance.SetEditorServiceAccountEmail("projectlf-19839882@appspot.gserviceaccount.com");
        FirebaseApp.DefaultInstance.SetEditorP12Password("notasecret");
#endif
    }

    public void UpLoadFb()
    {
        FBDBUserData SaveData = new FBDBUserData();
        SaveData.UserNick = string.Empty;
        SaveData.Gold = GameData.Gold;
        SaveData.Cash = GameData.Cash;

        FirebaseDatabase FBDB = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance);
        DatabaseReference FBDB_Reference = FBDB.GetReferenceFromUrl(FirebaseApp.DefaultInstance.GetEditorDatabaseUrl() + FB_User.UserId);

        UpLoadTask = FBDB_Reference.SetRawJsonValueAsync(JsonUtility.ToJson(SaveData));
        UpLoadState = FBDB_UpLoadState.UpLoad_UserData;
    }
    
    public void CheckData()
    {
        FirebaseDatabase FBDB = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance);
        DatabaseReference FBDB_Reference = FBDB.GetReferenceFromUrl(FirebaseApp.DefaultInstance.GetEditorDatabaseUrl() + FB_User.UserId);

        LoadTask = FBDB_Reference.GetValueAsync();
        LoadState = FBDB_LoadState.Check_UserData;
    }

    public void LoadCash()
    {
        FirebaseDatabase FBDB = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance);
        DatabaseReference FBDB_Reference = FBDB.GetReferenceFromUrl(FirebaseApp.DefaultInstance.GetEditorDatabaseUrl() + FB_User.UserId + "Cash");

        LoadTask = FBDB_Reference.GetValueAsync();
        LoadState = FBDB_LoadState.Load_Cash;
    }

    public void RubySet()
    {
        FirebaseDatabase FBDB = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance);
        DatabaseReference FBDB_Reference = FBDB.GetReferenceFromUrl(FirebaseApp.DefaultInstance.GetEditorDatabaseUrl() + FB_User.UserId);

        Dictionary<string, object> data = new Dictionary<string, object>();
        data.Add("Cash", GameData.Cash);
        UpLoadTask = FBDB_Reference.UpdateChildrenAsync(data);
        UpLoadState = FBDB_UpLoadState.UpLoad_Ruby;
    }

    public void LoadUserData()
    {
        FirebaseDatabase FBDB = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance);
        DatabaseReference FBDB_Reference = FBDB.GetReferenceFromUrl(FirebaseApp.DefaultInstance.GetEditorDatabaseUrl() + Social.localUser.id);

        LoadTask = FBDB_Reference.GetValueAsync();
        LoadState = FBDB_LoadState.Load_UserData;
    }

    void UnPackUserData(DataSnapshot data_)
    {
        FBDBUserData userdata = JsonUtility.FromJson<FBDBUserData>(data_.GetRawJsonValue());
    }

    void VersionCheck()
    {
        FirebaseDatabase FBDB = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance);
        DatabaseReference FBDB_Reference = FBDB.GetReferenceFromUrl(FirebaseApp.DefaultInstance.GetEditorDatabaseUrl() + "Version");

        LoadTask = FBDB_Reference.GetValueAsync();
        LoadState = FBDB_LoadState.CheckVersion;
    }

    public void PushOn()
    {
        Firebase.Messaging.FirebaseMessaging.Subscribe("/topics/ProjectLF_Message");
    }

    public void PushOFF()
    {
        Firebase.Messaging.FirebaseMessaging.Unsubscribe("/topics/ProjectLF_Message");
    }
    //fcm recive//
    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
        //sdogjksdf9
    }
    //선언만 해두면 된다(처리는 필요없음 푸시는 오기만 하면 됨)
    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        //UnityEngine.Debug.Log("From: " + e.Message.From);
        //UnityEngine.Debug.Log("Message ID: " + e.Message.MessageId);
        //foreach (KeyValuePair<string, string> o in e.Message.Data)
        //{
        //    Debug.Log("data key = " + o.Key);
        //    Debug.Log("data.value = " + o.Value);
        //}
    }

    private void Update()
    {
        if (UpLoadTask != null && UpLoadTask.IsCompleted)
        {
            switch (UpLoadState)
            {
                case FBDB_UpLoadState.UpLoad_Ruby:
                    break;
                case FBDB_UpLoadState.UpLoad_UserData:
                    break;
            }
            Debug.Log("upload prosess complete");
            UpLoadTask = null;
            UpLoadState = FBDB_UpLoadState.None;
        }

        if (LoadTask != null && LoadTask.IsCompleted)
        {
            switch (LoadState)
            {
                case FBDB_LoadState.Check_UserData:
                    if (LoadTask.Result.Value == null)
                    {
                        UpLoadFb();
                    }
                    break;
                case FBDB_LoadState.CheckVersion:
                    Debug.Log("version = " + LoadTask.Result.Value.ToString());
                    break;
                case FBDB_LoadState.Load_Cash:
                    break;
                case FBDB_LoadState.Load_UserData:
                    UnPackUserData(LoadTask.Result);
                    break;
            }
            Debug.Log("load prosess complete");
            LoadTask = null;
            LoadState = FBDB_LoadState.None;
        }
    }

}
