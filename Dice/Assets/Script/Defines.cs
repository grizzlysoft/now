﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//싱글톤 소스
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T instance = null;
    public static T Get
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(T)) as T;

                if (instance == null)
                {
                    Debug.Log("Nothing" + instance.ToString());
                    return null;
                }
            }
            return instance;
        }
    }
}

public static class UtilityTools
{
    public enum GradeType
    {
        Common,
        UnCommon,
        Magic,
        Rare,
        Unique,
        Relic,
        Legendary
    }

    public static Color UI_Color_Normal;
    public static Color UI_Color_DeleteItem;
    public static Color UI_Color_SellItem;
    public static Color UI_Color_Select;
    public static Color UI_Color_EmptySlot;

    public static Color Item_Color_Common;
    public static Color Item_Color_UnCommon;
    public static Color Item_Color_Magic;
    public static Color Item_Color_Rare;
    public static Color Item_Color_Unique;
    public static Color Item_Color_Relic;
    public static Color Item_Color_Legendary;

    public static void SetColor()
    {
        UI_Color_Normal     = HexToColor("787878FF");
        UI_Color_DeleteItem = HexToColor("9C1515FF");
        UI_Color_SellItem   = HexToColor("E9EC00FF");
        UI_Color_Select     = HexToColor("00FF00FF");
        UI_Color_EmptySlot  = HexToColor("3C3C3CFF");

        Item_Color_Common   = HexToColor("BDBDBDFF");
        Item_Color_UnCommon = HexToColor("48C524FF");
        Item_Color_Magic    = HexToColor("568CD6FF");
        Item_Color_Rare     = HexToColor("D7DA1BFF");
        Item_Color_Unique   = HexToColor("B33FCCFF");
        Item_Color_Relic    = HexToColor("CA701EFF");
        Item_Color_Legendary= HexToColor("830000FF");
    }

    public static Color GetGradeColor(GradeType _type)
    {
        switch (_type)
        {
            case GradeType.Common:   return Item_Color_Common;
            case GradeType.UnCommon: return Item_Color_UnCommon;
            case GradeType.Magic:    return Item_Color_Magic;
            case GradeType.Rare:     return Item_Color_Rare;
            case GradeType.Unique:   return Item_Color_Unique;
            case GradeType.Relic:    return Item_Color_Relic;
            case GradeType.Legendary:return Item_Color_Legendary;
            default:                 return Item_Color_Common;
        }
    }

    public static string GetGradeText(GradeType _type)
    {
        switch (_type)
        {
            case GradeType.Common: return "일반";
            case GradeType.UnCommon: return "고급";
            case GradeType.Magic: return "마법";
            case GradeType.Rare: return "희귀";
            case GradeType.Unique: return "영웅";
            case GradeType.Relic: return "유물";
            case GradeType.Legendary: return "전설";
            default: return "일반";
        }
    }

    /// 소수점을 제거하는 거
    static string RemoveFloat(string value)
    {
        int findIndex = 0;
        for (int i = 0; i < value.Length; i++)
        {
            if (value[i] == '.')
            {
                findIndex = i;
                break;
            }
        }

        if (findIndex != 0)
        {
            return value.Substring(0, findIndex);
        }

        return value;
    }

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, a);
    }
}

#region 딜리게이트 선언

public delegate void Del_Void();
public delegate void Del_Int(int _value);
public delegate void Del_String(string _value);


#endregion

#region 전역 클래스
public class Debuff
{
    public enum Type
    {
        Poison,
        Burns,
        Bleeding,

        Stun,
        Sleep,
        StoneCurse,
        Freeze,
        Fear
    }

    //지속시간
    public int remainTurn;
    public Type type;
}

public class Inven_Character
{
    public int ID;
    public int Level;
}

public class Inven_Item
{
    public int ID;
    public int Count;
}
#endregion

#region Enum
public enum FBDB_UpLoadState
{
    None,
    UpLoad_UserData,
    UpLoad_Ruby,
}

public enum FBDB_LoadState
{
    None,
    Check_UserData,
    Load_Cash,
    Load_UserData,
    CheckVersion,
}

public enum PayType
{
    ADS,
    CASH,
    DIA,
    GOLD,
    Mileage,
}

public enum RewardType
{
    Dia,
    Gold,
    Stamp,
    Pass,
    Mileage,
}

public enum ShopTap
{
    Package,
    Dia,
    Gold,
    Mileage,
    Stamp,
    Pass,
    Dice,
    Costum,
    RandomBox,
}

public enum RegionType
{
    Greicia,
    Crimson,
    Vestia,
    Minero,
}
public enum ClassType
{
    Tanker,
    Dealer,
    Healer
}

public enum DiceType
{
    d4 = 4,
    d6 = 6,
    d8 = 8,
    d10 = 10,
    d12 = 12,
    d20 = 20
}

public enum AchievementType
{
    Max,
}
#endregion

#region FBDBClass
public class FBDBUserData
{
    public string UserNick;
    public int Gold;
    public int Cash;
}
#endregion 
