﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownUI : MonoBehaviour
{
    public UILabel GoldLabel;
    public UILabel CashLabel;
    public UIGrid Gpgs_Grid;

    public GameObject[] Gpgs_Btns = new GameObject[4];

    bool GpgsOpen = false;

    public void Start()
    {
        GameData.Gold_Del += GoldLabelUpdate;
        GameData.Cash_Del += CashLabelUpdate;

        GoldLabelUpdate(GameData.Gold);
        CashLabelUpdate(GameData.Cash);
    }

    public void OnClickedGpgsBtn()
    {
        if(!GpgsOpen)
        {
            Gpgs_Grid.gameObject.SetActive(true);
            Gpgs_Grid.Reposition();
            GpgsOpen = true;
        }
        else
        {
            GpgsOpen = false;
            Gpgs_Grid.gameObject.SetActive(false);
            for (int i = 0; i < Gpgs_Btns.Length; ++i)
                Gpgs_Btns[i].transform.localPosition = Vector3.zero;
        }
    }

    void GoldLabelUpdate(int gold_)
    {
        GoldLabel.text = string.Format("{0:##,##0}", gold_);
    }

    void CashLabelUpdate(int cash_)
    {
        CashLabel.text = string.Format("{0:##,##0}", cash_);
    }

    private void OnDestroy()
    {
        GameData.Gold_Del -= GoldLabelUpdate;
        GameData.Cash_Del -= CashLabelUpdate;
    }
}
