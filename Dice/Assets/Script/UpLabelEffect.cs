﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpLabelEffect : MonoBehaviour 
{
    [SerializeField] UILabel label;

    public void Show(Vector3 _pos, string _text)
    {
        this.gameObject.transform.localPosition = _pos;
        label.text = _text;
    }


    public void Hide()
    {
        Destroy(this.gameObject);
    }
}
