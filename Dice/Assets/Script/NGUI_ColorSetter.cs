﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class NGUI_ColorSetter : MonoBehaviour 
{
    public Color color = Color.white;
    Color beforeColor = Color.white;
    public List<UISprite> li_Part = new List<UISprite>();

    //얼굴 <
    public UISprite sp_Face;
    string CharacterName;
    public float nowFace;
    float beforeFace = 0.0f;


    public void Init(string _name)
    {
        CharacterName = _name;
    }

    private void Update()
    {
        if(color != beforeColor)
        {
            for (int i = 0; i < li_Part.Count; i++)
            {
                li_Part[i].color = color;
            } 

            beforeColor = color;
        }

        if (nowFace != beforeFace)
        {
            sp_Face.spriteName = string.Format("{0}_0_face_{1}", CharacterName, GetName());
            beforeFace = nowFace;
        }
    }

    string GetName()
    {
        if (nowFace == 1.0f)
            return "Hit";
        else
            return "Idle";
    }
}
